export class Product {
    constructor(
        public id: String,
        public title: String, 
        public desc: String,
        public url: String, 
        public actualPrice: Number, 
        public discountPrice: Number, 
        public hasDiscount: Boolean) {}
}