import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product';

declare var $: any;

@Component({
  selector: 'app-product-group',
  templateUrl: './product-group.component.html',
  styleUrls: ['./product-group.component.css']
})
export class ProductGroupComponent implements OnInit {

  @Input('product') product: Product;

  constructor() { }

  ngOnInit() {

    $(document).ready(function () {
      if ($('.isotope-grid').length) {
        var $grid = $('.isotope-grid').imagesLoaded(function () {
          $grid.isotope({
            itemSelector: '.grid-item',
            transitionDuration: '0.7s',
            masonry: {
              columnWidth: '.grid-sizer',
              gutter: '.gutter-sizer'
            }
          });
        });
      }
    });
  }

}
