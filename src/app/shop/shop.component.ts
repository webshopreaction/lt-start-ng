import { Component, OnInit } from '@angular/core';
import { Product } from './product';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  products: Array<any> = [];
  viewToggle: Boolean = false;

  categories: Array<String> = [
    "Ortopediset tuet",
    "Vesityynyt Tyynyliinat",
    "Kuorsaus Uniapnea",
    "Migreeni-tuotteet",
    "Astma Allergia",
    "Tuotteet jaloille",
    "Kivunlievitys tuotteet",
    "Hygienia Kauneus",
    "Pilates &amp; Jooga",
    "Kuntoutus tuotteet",
    "Silikonituet jaloille",
    "Liikuntavälineet",
    "Lihashuolto Hieronta",
    "Laihduttaminen",
    "Mittausvälineet",
    "Hampaiden valkaisu",
    "Tuotteet kotihoitoon",
    "Tyyny-tuotteet",
    "Omron tuotteet",
    "Voimistelumatot",
    "Flexibar Flexibak",
    "Lonkkasuoja-housut",
    "Mediflow tyyny",
    "Kotimaiset satulatuolit",
    "Kippilaitteet selälle",
    "Eläintuotteet",
    "Jumppanauhat",
    "HighRoller-laite",
    "Kinesioteippaus",
    "Kuumavesipullot",
    "Tuotteet lapsille",
    "Äitiys-tuotteet",
    "ClipAir-nenäliuskat",
    "Cleane aknehoito",
    "Roll Easy-apuväline",
    "Somnolis-kisko",
    "Pulssioksimetri",
    "Ravintolisät",
    "Kipukynä",
    "Piikkimatto",
    "Patjat",
    "Diabetes tuotteet",
    "Spinegym",
    "Pilatesrulla",
    "LabTex - diili",
    "LabTex - diili II"
  ]

  constructor() { }

  ngOnInit() {
    this.loadProducts();
  }

  loadProducts() {
    var products: Product[] = [
      new Product("1", "Labtex Care Nordic", "Some description about the product Some description about the product Some description about the product", "assets/products/cat1/1.jpg", 49.95, 0, false),
      new Product("2", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/2.jpg", 49.95, 0, false),
      new Product("3", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/3.jpg", 49.95, 0, false),
      new Product("4", "Labtex Care Nordic", "Some description about the product Some description about the product Some description about the product Some description about the product", "assets/products/cat1/4.png", 49.95, 0, false),
      new Product("5", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/5.jpg", 49.95, 0, false),
      new Product("6", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/6.jpg", 49.95, 25.95, true),
      new Product("7", "Labtex Care Nordic", "Some description about the product Some description about the product", "assets/products/cat1/7.jpg", 49.95, 0, false),
      new Product("8", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/8.jpg", 49.95, 0, false),
      new Product("9", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/9.jpg", 49.95, 0, false),
      new Product("10", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/10.jpg", 49.95, 0, false),
      new Product("11", "Labtex Care Nordic", "Some description about the product", "assets/products/cat1/5.jpg", 49.95, 25.95, true),
    ];

    //console.log(this.product_group);
    this.products = products;
  }

  toggleView() {
    this.viewToggle = !this.viewToggle;
  }

}
