import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LtInfoComponent } from './lt-info.component';

describe('LtInfoComponent', () => {
  let component: LtInfoComponent;
  let fixture: ComponentFixture<LtInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LtInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LtInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
