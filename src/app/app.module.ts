import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CaroselComponent } from './carosel/carosel.component';
import { HomeCardComponent } from './home-card/home-card.component';
import { SubNavComponent } from './sub-nav/sub-nav.component';
import { HomeInfoComponent } from './home-info/home-info.component';
import { DocComponent } from './home-info/doc/doc.component';
import { OfferComponent } from './home-info/offer/offer.component';
import { LtInfoComponent } from './home-info/lt-info/lt-info.component';
import { FooterComponent } from './footer/footer.component';
import { ShopComponent } from './shop/shop.component';
import { HomeComponent } from './home/home.component';
import { PageTitleComponent } from './common/page-title/page-title.component';
import { ProductSingleComponent } from './shop/product-single/product-single.component';
import { ProductGroupComponent } from './shop/product-group/product-group.component';
import { ProductListComponent } from './shop/product-list/product-list.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    CaroselComponent,
    HomeCardComponent,
    SubNavComponent,
    HomeInfoComponent,
    DocComponent,
    OfferComponent,
    LtInfoComponent,
    FooterComponent,
    ShopComponent,
    HomeComponent,
    PageTitleComponent,
    ProductSingleComponent,
    ProductGroupComponent,
    ProductListComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent },
      { path: 'shop', component:ShopComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
